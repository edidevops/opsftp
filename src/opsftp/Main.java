package opsftp;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import javax.swing.text.StyledDocument;

/**
 *
 * @author i.cornish
 *
 *
 */
public class Main {
 
    private Statement stmt;
    private String passphrase;
    private String site = "";
//    private boolean crccheck = false;
    private String[] serverconf;
    private boolean wasError = false;
    private boolean CRCError = false;
    private boolean updown = true;
    private String fileext = "";
    private boolean filefilter = false;
    /* the following vars should be populated using
     * the FTPServer.cfg configuration file, base on
     * the site parameter passed in the command line
     */
    private String host = "";
    private String user = "";
    private String pass = "";
    private int port = 22;

    /* the following vars should be populated by the
     * praseCommandLine
     */
    private String logfile = "o:\\AUTO\\logs\\OpsFtp.log";
    private String localpath = "o:\\AUTO\\Configs\\";
    private String remotepath = "/idctest/";
    private progress prog = new progress();
    private String privateKey = "";
  
    //private progress prog2 = new progress();

    Session     session     = null;
    Channel     channel     = null;
    ChannelSftp channelSftp = null;

    /* public construction method */
    public Main(String[] args) {

        praseCommandLine(args);

        prog.setTitle(this.host);

        //this.setProgressMonitor( new ProgressMonitor() );// del

        // Display the progress window
        //prog2.setVisible(true);
        //prog2.setProgressText("Setting up, Please wait...");
        prog.setVisible(true);
        String command="";
        prog.setProgressText2("[]<--\t"+command);
        prog.setProgressText("Setting up, Please wait...");
        prog.progressText_Label1.setText("[]<--\t"+this.host);

        if (updown == false) {
            getRemoteFiles();
        } else{
            pushRemoteFiles();
        }
        
    }

    private void getRemoteFiles() {
        try {
            
            JSch jsch = new JSch();
            
             if(port == 22){
                if( privateKey.equalsIgnoreCase("")){
             jsch.addIdentity(privateKey);
            }else{
                    //throw exception
                }
             }
            session = jsch.getSession(user, host, port);
            session.setPassword(pass);
            if (!passphrase.equalsIgnoreCase("")){
             UserInfo ui = new MyUserInfo(passphrase);
             session.setUserInfo(ui);
            };
   
            wasError = false;
            prog.setVisible(true);
            prog.setProgressText("Setting up, Please wait...");

            // set up connection details and connect to server
            prog.setProgressText("Connecting to remote FTP server");
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;
            channelSftp.cd(remotepath);
            // Get remote directory listing
            Vector remoteDirList = channelSftp.ls(remotepath);

            for (int i = 0; i < remoteDirList.size(); i++) {
                //get(localpath+remoteDirList[i] , remotepath+remoteDirList[i] );

                File localFile = new File(localpath + remoteDirList.get(i));
                String t = localFile.toString();

                boolean doCrc = false;

                t = t.substring(this.localpath.length());
                if (filefilter) {
                    if (remoteDirList.get(i).toString().toUpperCase().endsWith(fileext.toUpperCase())) {
                        channelSftp.get(localpath + remoteDirList.get(i), remotepath + remoteDirList.get(i));
                        if (hasIntegrity(localpath + remoteDirList.get(i), remotepath + remoteDirList.get(i)))
                        {
                            appendPane(remotepath + remoteDirList.get(i));
                        }
                        else{
                            appendPane("Error: there is file corruption "+ localpath + remoteDirList.get(i));
                        }
                        doCrc = true;
                    }
                } else {
                    channelSftp.get(localpath + remoteDirList.get(i), remotepath + remoteDirList.get(i));

//                if (hasIntegrity(localpath + remoteDirList.get(i), remotepath + remoteDirList.get(i)))
//                        {
                            appendPane(remotepath + remoteDirList.get(i));
                        //}
//                        else{
//                            appendPane("Error: there is file corruption "+ localpath + remoteDirList.get(i));
//                        }


                    doCrc = true;
                }

                // Do we need tocheck the CRC values of the two files?
            }
            
                // Disconnect from remote FTP server
                prog.setProgressText("Disconnecting from FTP Server...");

                // display finished message and wait for 3 seconds
                prog.setProgressText("FINISHED!");
                Thread.sleep(3000);
       
                channelSftp.exit();
                session.disconnect();

                    System.exit(0);

          
        } catch (JSchException ex) {
            appendLog("JSchException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
            System.exit(1);
        } catch (SftpException ex) {
            appendLog("SftpException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
            System.exit(1);        } 
        catch (InterruptedException ex) {
            appendLog("InterruptedException: " + ex.getMessage());
            System.exit(1);        }
    }
    
    private void pushRemoteFiles(){
        
        try {
            
            JSch jsch = new JSch();
            session = jsch.getSession(user, host, port);

             if(port == 22){
                if(!privateKey.equalsIgnoreCase("")){
             jsch.addIdentity(privateKey, passphrase);
            }   else{
                    //throw exception
                    
                    throw new PrivateKeyException("No Private Key Exception");
                }
             }  else {
                session.setPassword(pass);
             }
            
   
            wasError = false;
            prog.setVisible(true);
            prog.setProgressText("Setting up, Please wait...");

            // set up connection details and connect to server
            prog.setProgressText("Connecting to remote FTP server");
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;
            channelSftp.cd(remotepath);
            
            File checkLocalPath = new File(localpath);
                if (!checkLocalPath.exists()) {
                    appendLog("The local folder '" + localpath + "' does not exist");
                    System.exit(1);
                }
            
            prog.setProgressText("Building upload file list for " + localpath + ", Please wait...");
    
            File[] localFiles = new File(localpath).listFiles();

                // Loop thought each of the file in the localFile array
                for (int i = 0; i < localFiles.length; i++) {

                // if the element within localFiles[] is not
                // a directory and is a file then we will process
                // the item as a file.
                if (!localFiles[i].isDirectory() && localFiles[i].isFile()) {

                    prog.setProgressText("Checking if file nedded uploading...");
                    boolean wasFileUploaded = false, chkFileUploaded = false;

                    String t = localFiles[i].toString();
                    t = t.substring(this.localpath.length());

                    // if the filefilter is true then we need to check if the file
                    // matches the filefilter parameter before uploading.
                
            // Get remote directory listing

             if (filefilter==true) {
                        if (localFiles[i].toString().toUpperCase().endsWith(fileext.toUpperCase())) {
                            prog.setProgressText("Uploading '" + localFiles[i].toString() + "'");
                            channelSftp.put(localFiles[i].toString(), remotepath + t, ChannelSftp.OVERWRITE);
//  if (hasIntegrity(localFiles[i].toString(), remotepath + t))
//                        {
                            appendPane(remotepath + t + '\n');
                        //}
//                        else{
//                            appendPane("Error: there is file corruption "+ localpath + remotepath + t);
//                        }
                            wasFileUploaded = true;
                            chkFileUploaded = true;
                        }else{
                            wasFileUploaded = false;
                            chkFileUploaded = true;
                        }
                        }else{
                            prog.setProgressText("Uploading '" + localFiles[i].toString() + "'");
                            channelSftp.put(localFiles[i].toString(), remotepath + t, ChannelSftp.OVERWRITE);
 // if (hasIntegrity(localFiles[i].toString(), remotepath + t))
             //           {
                            appendPane("\n" + remotepath + t);
                     //   }
//                        else{
//                            appendPane("Error: there is file corruption "+ localpath + remotepath + t +'\n');
//                        }
                            wasFileUploaded = true;
                            chkFileUploaded = true;
                        }
                    }
                }
                // Disconnect from remote FTP server
                prog.setProgressText("Disconnecting from FTP Server...");
                // display finished message and wait for 3 seconds
                prog.setProgressText("FINISHED!");
                Thread.sleep(3000);
        channelSftp.exit();
        session.disconnect();
                         System.exit(0);
        } catch (JSchException ex) {
            appendLog("JSchException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
            System.exit(1);
        } catch (SftpException ex) {
            appendLog("SftpException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
            System.exit(1);        } 
        catch (PrivateKeyException ex) {
            appendLog("PrivateKeyException: " + ex.getMessage());
            System.exit(1);
        } catch (InterruptedException ex) {
            appendLog("InterruptedException: " + ex.getMessage());
        }
    }
    
    private void praseCommandLine(String[] args) {
        if (args.length == 0) {
            appendLog("Parameters not found");
        }

        int chk = 0;
        for (int i = 0; i < args.length; i++) {

            if (args[i].compareToIgnoreCase("-site") == 0) {
                this.site = args[i + 1];

                // read ftp site config
                loadFTPConfig();
                for (int x = 1; x < serverconf.length; x++) {
                    
                    String hostName = serverconf[x].substring(0, this.site.length());
                    if (hostName.equalsIgnoreCase(this.site)) {
                        // do something here
                        //appendLog("Found the site configuration line in FTPServer.cfg");

                        String temp[] = serverconf[x].split("\t");
                        this.host = temp[1];
                        this.user = temp[2];
                        this.pass = temp[3];
                        break;
                    }
                    else{
                       System.out.println(this.site.toUpperCase()); 
                       String temp[] = serverconf[x].split("\t");
                       
                    }
                }
            }

            if (args[i].compareToIgnoreCase("-local") == 0) {
                this.localpath = args[i + 1];
            }
            
            if (args[i].compareToIgnoreCase("-sshpass") == 0) {
                this.passphrase = args[i + 1];
            }
            if (args[i].compareToIgnoreCase("-user") == 0) {
                this.user = args[i + 1];
            }

            
            if (args[i].compareToIgnoreCase("-key") == 0) {
                this.privateKey = args[i + 1];
            }

            if (args[i].compareToIgnoreCase("-download") == 0) {
                this.updown = false;
            }

            if (args[i].compareToIgnoreCase("-remote") == 0) {
                this.remotepath = args[i + 1];
            }

            if (args[i].compareToIgnoreCase("-port") == 0) {
                try {
                    this.port = Integer.parseInt(args[i + 1]);
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    System.exit(1);
                }
            }

            if (args[i].compareToIgnoreCase("-log") == 0) {
                try {
                    this.logfile = args[i + 1];
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    System.exit(1);
                }
            }
            
        }
        
        try{
            //checking for db entry.
         
        }catch(Exception e){
            //appendLog("" e.getMessage());
        }
    }

    /* load FTPServer config */
    private void loadFTPConfig() {
        try {
            BufferedReader in = new BufferedReader(new FileReader("O:\\AUTO\\Configs\\FTPServers.cfg"));
            String str;
            int linecount = 0;
            while ((str = in.readLine()) != null) {
                linecount++;
                //serverconf[xxx] = str;

            }
            serverconf = new String[linecount + 1];
            in.close();

        } catch (IOException e) {
        }

        try {
            BufferedReader in = new BufferedReader(new FileReader("O:\\AUTO\\Configs\\FTPServers.cfg"));
            String str;
            int currentline = 0;
            while ((str = in.readLine()) != null) {
                currentline++;
                serverconf[currentline] = str;
            }
            in.close();

        } catch (IOException e) {
        }
    }

    /* public method for producing a crc32 on a local file check */
    private String getCRC(File fileName) {
        try {
            CheckedInputStream cis = null;
            //long filesize = 0;
            // Computer CRC32 checksum
            cis = new CheckedInputStream(new FileInputStream(fileName.toString()), new CRC32() );
            //filesize = fileName.length();            

            byte[] buf = new byte[128];
            while ( cis.read(buf)>=0) {
            }
            
            long checksum = cis.getChecksum().getValue();
           // System.out.println(checksum + " " + filesize + " " + fileName);

            return String.valueOf(checksum);

        } catch (Exception e) {
            appendLog("getCRC: " + e.getMessage());
            //e.printStackTrace();
        }
        return null;
    }


    /* append a message to the OPSFTP log file */
    private void appendLog(String logMessage) {
        try {
            // Build DateTime String
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    java.util.Date now = new java.util.Date();
	    String sDate= sdf.format(now);

            BufferedWriter out = new BufferedWriter(new FileWriter(logfile, true));
            out.write(sDate + "\t" + logMessage + "\r\n");
            out.close();
        } catch (IOException e) {
        }
    }
    
private boolean getFileType(String fname){
    fname = fname.toLowerCase();
    if (fname.endsWith(".zip"))
        return true;
    if (fname.endsWith(".exe"))
        return true;
    if (fname.endsWith(".pdf"))
        return true;
    if (fname.endsWith(".z01"))
        return true;
    if (fname.endsWith(".z02"))
        return true;
    if (fname.endsWith(".z03"))
        return true;
    if (fname.endsWith(".tar"))
        return true;
    if (fname.endsWith(".gz"))
        return true;
    if (fname.endsWith(".xls"))
        return true;

    //check for extension between 0 to 999
    try{
        String [] tmpext = fname.split("\\.");
        if(Integer.parseInt(tmpext[tmpext.length-1]) > 0 && Integer.parseInt(tmpext[tmpext.length-1]) <= 999){
            return true;
        }
    }catch(Exception e){
    }

    return false;
}
    /* public bootstrap method, this is the java VM entry point */
    public static void main(String[] args) {
        new Main(args);
    }
    
        public void appendPane(String text){
        
        StyledDocument doc = prog.progressText_Label1.getStyledDocument();
        
        try
        {
            doc.insertString(doc.getLength(), text+"\n", null );
        }
        catch(Exception e) { System.out.println(e); }
        
    }
        
        private boolean hasIntegrity(String local, String remote){
            
            File localFile = new File(local);
            File remoteFile = new File(remote);
    
        try {
            
            //Use MD5 algorithm
            MessageDigest md5Digest = MessageDigest.getInstance("MD5");
 
            //Get the checksum
            String localCheckSum = getFileChecksum(md5Digest, localFile);
            
            String remoteCheckSum = getFileChecksum(md5Digest, remoteFile);

            if (localCheckSum.equalsIgnoreCase(remoteCheckSum))
            {
                return true;
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
          return false;  
        }
        
        

     private  String getFileChecksum(MessageDigest digest, File file) throws IOException
{
    //Get file input stream for reading the file content
    FileInputStream fis = new FileInputStream(file);
     
    //Create byte array to read data in chunks
    byte[] byteArray = new byte[1024];
    int bytesCount = 0;
      
    //Read file data and update in message digest
    while ((bytesCount = fis.read(byteArray)) != -1) {
        digest.update(byteArray, 0, bytesCount);
    };
     
    //close the stream; We don't need it now.
    fis.close();
     
    //Get the hash's bytes
    byte[] bytes = digest.digest();
     
    //This bytes[] has bytes in decimal format;
    //Convert it to hexadecimal format
    StringBuilder sb = new StringBuilder();
    for(int i=0; i< bytes.length ;i++)
    {
        sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
    }
     
    //return complete hash
   return sb.toString();
}   
        
}

class MyUserInfo implements UserInfo{
    
private final String passphrase;

    public MyUserInfo(String passphrase) {
        this.passphrase = passphrase;
    }

    @Override
    public String getPassword() {
return null;    }

    @Override
    public boolean promptPassword(String string) {
return false;    }

    @Override
    public boolean promptPassphrase(String string) {
return false;    }

    @Override
    public boolean promptYesNo(String string) {
return false;    }

    @Override
    public void showMessage(String string) {
    }

    @Override
    public String getPassphrase() {
return passphrase;    }

    
    
    
    
}
